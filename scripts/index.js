// [ Теоретичні питання ]
//1) setTimeout() викликає функцію один раз через заданий час, а setInterval() - регулярно через деякий заданий інтервал часу
//2) функція стане в чергу й буде там допоки не виконаються всі операції в браузері, після чого спрацює (не миттєво, проте швидко)
//3) бо при виклику setTimeout() значення з функції буде повертатись нескінченно (до тих пір, пока її не зупинити)

const img = document.querySelectorAll('img')
let counter = 0;
let interval = null;

const btnSecond = document.createElement('button')
btnSecond.className = 'btn-second'
btnSecond.innerText = 'відновити'
 document.body.appendChild(btnSecond)

btnSecond.addEventListener('click', () => {
  interval = setInterval(() => {

  img[counter].classList.remove('image-to-show')
  counter++
  if (counter === img.length) {
    counter = 0;
  }
  img[counter].classList.add('image-to-show')
 }, 3000);

})

const btn = document.createElement('button')
btn.className = 'btn'
btn.innerText = 'припинити'
document.body.appendChild(btn)

btn.addEventListener('click', () => {
  clearInterval(interval)
})
 

